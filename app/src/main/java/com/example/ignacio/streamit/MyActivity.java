package com.example.ignacio.streamit;

import android.content.Intent;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class MyActivity extends ActionBarActivity implements SurfaceHolder.Callback {

   // User Interface Elements
   // Video variable
   // Networking variables
   // public static String SERVERIP = "";
   // public static final int SERVERPORT = 6775;
   // private ServerSocket serverSocket;
   boolean recording;
   //public static final int SERVERPORT = 62101;
  // public static String SERVERIP="130.206.163.6"; //NUESTRO SERVIDOR
    public static final int SERVERPORT = 6675;
    public static String SERVERIP="192.168.43.195";
    Socket clientSocket;
    //private Camera mCamera;
    SurfaceView mView;
    TextView connectionStatus;
    SurfaceHolder mHolder;
    MediaRecorder recorder;
    private Handler handler = new Handler();
    /**
     * Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        // Define UI elements

        connectionStatus = (TextView) findViewById(R.id.textView);
        mView = (SurfaceView) findViewById(R.id.surfaceView);
        mHolder = mView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


        final Thread sendVideo = new Thread(new SendVideoThread());
        try {
            sendVideo.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sendVideo.start();
   //  mView.setOnClickListener(new View.OnClickListener() {
     //       @Override
       //      public void onClick(View view) {
         //       sendVideo.stop();
           //     finish();
          //  }
       // });
         mHolder.addCallback((SurfaceHolder.Callback) this);
    }

    public class SendVideoThread implements Runnable {
        public void run() {
           try {
                if (SERVERIP != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            connectionStatus.setText("Connecting to IP: " + SERVERIP);
                        }
                    });
                    clientSocket = new Socket(SERVERIP,SERVERPORT);
                    clientSocket.setTcpNoDelay(true);
                        //listen for incoming clients

             //       if(mCamera == null) {
               //         mCamera = Camera.open();
                 //       mCamera.unlock();
                  //  }
                       // handler.post(new Runnable() {
                         //   @Override
                           // public void run() {
                             //   connectionStatus.setText("Connected.");
                           // }
                       // });
                        try {
                            // Begin video communication
                            connectionStatus.setText("Connected.");
                            final ParcelFileDescriptor pfd = ParcelFileDescriptor.fromSocket(clientSocket);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {


                                    //mCamera.getCameraInstance();
                                    recorder = new MediaRecorder();


                                //    recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                                //    recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
                                //    recorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
                                //    recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
                                //    recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);

                                    recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
                                    recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                                    recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                                    recorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);
                                    recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

                                 //   recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                                 //   recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
                                 //   recorder.setOutputFormat(8);
                                 //   recorder.setAudioEncoder(3);
                                 //   recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
                                 //   recorder.setVideoFrameRate(20);
                                 //   recorder.setVideoSize(640, 480);
                                 //   recorder.setVideoEncodingBitRate(1024*1024);
                                 //   recorder.setOutputFile(pfd.getFileDescriptor());

                            //        recorder.setVideoFrameRate(20);
                              //      recorder.setVideoSize(176, 144);
                                    recorder.setOutputFile(pfd.getFileDescriptor());
                                    recorder.setPreviewDisplay(mHolder.getSurface());
                                    try {
                                        recorder.prepare();
                                    } catch (IllegalStateException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                    recorder.start();
                                    recording=true;

                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    connectionStatus.setText("Oops.Connection interrupted. Please reconnect your phones.");
                                }
                            });
                            e.printStackTrace();
                        }

                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            connectionStatus.setText("Couldn't detect internet connection.");
                        }
                    });
                }
            } catch (Exception e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        connectionStatus.setText("Error");
                    }
                });
                e.printStackTrace();
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

        if (recording) {
            recorder.stop();
            recording = false;
        }
        recorder.release();
        finish();
    }
}

// SERVERIP = "192.168.1.126";
// Run new thread to handle socket communications

//  Button button = (Button) findViewById(R.id.button);
//  button.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        startActivity(new Intent(MyActivity.this, Recibir.class));
//      }
//  });
//  Button button2 = (Button) findViewById(R.id.button2);
//  button2.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//          startActivity(new Intent(MyActivity.this, Enviaraudio.class));
//      }
//  });
//  Button button3 = (Button) findViewById(R.id.button3);
//  button3.setOnClickListener(new View.OnClickListener() {
//     @Override
//      public void onClick(View view) {
//          startActivity(new Intent(MyActivity.this, clientedelvideo.class));
//      }
//  });