package com.example.ignacio.streamit;

import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;


public class clientedelvideo extends ActionBarActivity implements SurfaceHolder.Callback{


    MediaPlayer mp;
    private SurfaceView mPreview;
    private SurfaceHolder holder;
    private TextView mTextview;
    public static final int SERVERPORT = 62111;
    public static String SERVERIP = "130.206.163.6";
    //public static final int SERVERPORT = 6676;
    //public static String SERVERIP = "192.168.43.195";
    private ServerSocket serverSocket;
    private Handler handler = new Handler();
    Socket clientSocket;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientedelvideo);
        mPreview = (SurfaceView) findViewById(R.id.surfaceView);
        mTextview = (TextView) findViewById(R.id.textView);
        holder = mPreview.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mTextview.setText("Attempting to connect");
        mp = new MediaPlayer();
        Thread t = new Thread() {
            public void run() {
                try {
                    clientSocket = new Socket(SERVERIP,SERVERPORT);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mTextview.setText("Connected to server");
                        }
                    });

                    //serverSocket = new ServerSocket(SERVERPORT);
                    //System.out.println("Checkpoint1");
                    //handler.post(new Runnable() {
                      //  @Override
                       // public void run() {
                        //    mTextview.setText("Connected to server");
                       // }
                    //});
                   // final Socket client = serverSocket.accept();
                   // System.out.println("Checkpoint2");
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ParcelFileDescriptor pfd = ParcelFileDescriptor.fromSocket(clientSocket);
                                pfd.getFileDescriptor().sync();
                                mp.setDataSource(pfd.getFileDescriptor());
                                pfd.close();
                                mp.setDisplay(holder);
                                mp.prepareAsync();
                                mp.start();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                        }
                    });

                } catch (UnknownHostException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }
}


