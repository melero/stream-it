package com.example.ignacio.streamit;

import android.annotation.TargetApi;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;


public class Enviaraudio extends ActionBarActivity {
    private Handler handler = new Handler();
    MediaRecorder recorder;
    String hostname = "130.206.163.6";
    int port = 8000;
    DatagramSocket socket = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviaraudio);

        Thread sendAudio = new Thread(new SendAudioThread());
        sendAudio.start();

    }

    public class SendAudioThread implements Runnable {
        @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        public void run() {
            // From Server.java


            try {
                socket = new DatagramSocket(port, InetAddress.getByName(hostname));
            } catch (IOException e) {
                e.printStackTrace();
            }

                // Begin video communication
                final ParcelFileDescriptor pfd = ParcelFileDescriptor.fromDatagramSocket(socket);
                handler.post(new Runnable() {
                    @Override
                    public void run() {


                        recorder = new MediaRecorder();
                        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                        recorder.setOutputFile(pfd.getFileDescriptor());

                        try {
                            recorder.prepare();
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        recorder.start();

                    }
                });


        }
    }
}